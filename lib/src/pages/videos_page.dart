import 'package:flutter/material.dart';
import 'package:VacunatePeru/src/widgets/menu_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;


class VideosPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Videos JA')
      ),
      drawer: MenuWidget(),
      body: FutureBuilder(
       future: http.get('https://www.youtube.com/c/JuniorAchievementPer%C3%BA/featured'),
       builder: (BuildContext context,snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator()) ;       
          } else{
              return Container(
                child: WebView(
                  
                      initialUrl: "https://www.youtube.com/c/JuniorAchievementPer%C3%BA/featured",
                      javascriptMode: JavascriptMode.unrestricted ,
                       
                      

                  
          ),
        );
        
          }


       }

      ),
    );
    
  }
  
 

}