import 'package:VacunatePeru/src/widgets/menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {

  String _nombre = '';
  String _email  = '';
  String _fecha  = '';

  String _opcionSeleccionada = 'Comas';

  List<String> _poderes = ['Comas','Rimac', 'Independencia', 'Ate', 'Surco', 'Miraflores', 'Molina','San Martín de Porres'];

  TextEditingController _inputFieldDateController = new TextEditingController();
  File foto;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Registro')),
                        leading: IconButton(
          iconSize:25,
          icon: const Icon(Icons.arrow_back_rounded),
          onPressed: () {Navigator.pushReplacementNamed(context, 'register');},
        ) ,
                actions: <Widget>[
          IconButton(
            icon: Icon( Icons.photo_size_select_actual ),
            onPressed: _seleccionarFoto,
          ),
          IconButton(
            icon: Icon( Icons.camera_alt ),
            onPressed: _tomarFoto,
          ),
        ],
      ),
      //drawer: MenuWidget(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _mostrarFoto(),
          Divider(),
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha( context ),
          Divider(),
          _enferMedades(),
          Divider(),

           _alergias(),
          Divider(),
                            _dni(),
          Divider(),

         _crearDropdown(),
          Divider(),
          _crearTitulo(),
         // _crearPersona()
        ],
        
      ),
    );
  }
  Widget _crearTitulo() {

    return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric( horizontal: 80.0, vertical: 15.0),
            child: Text('Registrar'),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          elevation: 0.0,
          color: Color(0xff00578b),
          textColor: Colors.white,
          onPressed: (){Navigator.pushReplacementNamed(context, 'alerta');},
        );

  }

  Widget _crearInput() {

    return TextField(
      // autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        //counter: Text('Letras ${ _nombre.length }'),
        //hintText: 'Nombre de la persona',
        labelText: 'Nombre',
        //helperText: 'Sólo es el nombre',
        suffixIcon: Icon( Icons.accessibility ),
        icon: Icon( Icons.account_circle )
      ),
      onChanged: (valor){
        setState(() {
          _nombre = valor;
        });
      },
    );

  }

  Widget _crearEmail() {

    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Email',
        labelText: 'Email',
        suffixIcon: Icon( Icons.alternate_email ),
        icon: Icon( Icons.email )
      ),
      onChanged: (valor) =>setState(() {
        _email = valor;
      })
    );

  }
  Widget _mostrarFoto() {

      //aqui hubo un cambio de una actualización
      if( foto != null ){
        return Image.file(
          foto,
          fit: BoxFit.cover,
          height: 300.0,
        );
      }
      return Container(
        height: 250,
              child:
              
              Image(image:AssetImage('assets/perfil.jpg')),     
         
      );
    

  }
  Widget _crearPassword(){

     return TextField(
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Password',
        labelText: 'Password',
        suffixIcon: Icon( Icons.lock_open ),
        icon: Icon( Icons.lock )
      ),
      onChanged: (valor) =>setState(() {
        _email = valor;
      })
    );

  }


  Widget _crearFecha( BuildContext context ) {

    return TextField(
      enableInteractiveSelection: false,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        hintText: 'Fecha de nacimiento',
        labelText: 'Fecha de nacimiento',
        suffixIcon: Icon( Icons.perm_contact_calendar ),
        icon: Icon( Icons.calendar_today )
      ),
      onTap: (){ 

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate( context );

      },
    );

  }
Widget _enferMedades() {

    return TextField(
      // autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        //counter: Text('Letras ${ _nombre.length }'),
        //hintText: 'Nombre de la persona',
        labelText: 'Enfermedades Persistentes',
        //helperText: 'Sólo es el nombre',
        suffixIcon: Icon( Icons.medical_services ),
        icon: Icon( Icons.medical_services_outlined )
      ),
      onChanged: (valor){
        setState(() {
          _nombre = valor;
        });
      },
    );

  }
Widget _dni() {

    return TextField(
      // autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        //counter: Text('Letras ${ _nombre.length }'),
        //hintText: 'Nombre de la persona',
        labelText: 'Dni o Pasaporte',
        //helperText: 'Sólo es el nombre',
        suffixIcon: Icon( Icons.contact_page_sharp ),
        icon: Icon( Icons.pages )
      ),
      onChanged: (valor){
        setState(() {
          _nombre = valor;
        });
      },
    );

  }

  Widget _alergias() {

    return TextField(
      // autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        //counter: Text('Letras ${ _nombre.length }'),
        //hintText: 'Nombre de la persona',
        labelText: 'Alergias',
        //helperText: 'Sólo es el nombre',
        suffixIcon: Icon( Icons.medical_services ),
        icon: Icon( Icons.medical_services_outlined )
      ),
      onChanged: (valor){
        setState(() {
          _nombre = valor;
        });
      },
    );

  }
  _selectDate(BuildContext context) async {

    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2020),
      lastDate: new DateTime(2025),
      locale: Locale('es', 'ES')
    );

    if ( picked != null ) {
      setState(() {
          _fecha = picked.toString();
          _inputFieldDateController.text = _fecha;
      }); 
    }

  }

  List<DropdownMenuItem<String>> getOpcionesDropdown() {

    List<DropdownMenuItem<String>> lista = new List();

    _poderes.forEach( (poder){

      lista.add( DropdownMenuItem(
        child: Text(poder),
        value: poder,
      ));

    });

    return lista;

  }

  Widget _crearDropdown() {

    return Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(width: 30.0),   
        Expanded(
          child: DropdownButton(
            value: _opcionSeleccionada,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                _opcionSeleccionada = opt;
              });
            },
          ),
        )

      ],
    );
    
    
    
    

  }



  Widget _crearPersona() {

    return ListTile(
      title: Text('Nombre es: $_nombre'),
      subtitle: Text('Email: $_email'),
      trailing: Text(_opcionSeleccionada),
    );

  }
_seleccionarFoto() async {

    _procesarImagen( ImageSource.gallery );

  }
  
  
  _tomarFoto() async {

    _procesarImagen( ImageSource.camera );

  }

  _procesarImagen( ImageSource origen ) async {
    //cambie con la actualización de la nota 224 del curso
    final _picker = ImagePicker();
 
    final pickedFile = await _picker.getImage(
      source: origen,
    );
    if (pickedFile == null) return null;// esto no esta en la solución 224 , pero lo acoplo como solución a cuando no se selecciona una imagen de la galería y ya no me salte un error.

    foto = File(pickedFile.path);
 
    /*if (foto != null) {
      producto.fotoUrl = null;
    }*/
 
    setState(() {});
  }
}