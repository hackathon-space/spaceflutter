
import 'dart:convert';


import 'package:http/http.dart' as http;


import 'package:VacunatePeru/src/models/podcast_model.dart';

class PodcastProvider {

  final String _url = 'https://vacunateperu-5dfe4-default-rtdb.firebaseio.com/';




  Future<List<PodCastModel>> cargarPodcast() async {

    final url  = '$_url/Podcast.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<PodCastModel> podcast = new List();


    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, podc ){

      final podcastTemp = PodCastModel.fromJson(podc);
      podcastTemp.id = id;

      podcast.add( podcastTemp );

    });

    // print( podcast[0].id );

    return podcast;

  }
}