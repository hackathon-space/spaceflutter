
import 'dart:convert';


import 'package:http/http.dart' as http;


import 'package:VacunatePeru/src/models/mapas_model.dart';

class MapaProvider {

  final String _url = 'https://vacunateperu-5dfe4-default-rtdb.firebaseio.com/';




  Future<List<MapasModel>> cargarMapa() async {

    final url  = '$_url/Mapa.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<MapasModel> mapas = new List();


    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, mapa ){

      final mapasTemp = MapasModel.fromJson(mapa);
      mapasTemp.id = id;

      mapas.add( mapasTemp );

    });

     //print( equipo[0].id );

    return mapas;

  }



  


}

