
import 'dart:convert';


import 'package:VacunatePeru/src/models/paciente_model.dart';
import 'package:http/http.dart' as http;



class PacienteProvider {

  final String _url = 'https://vacunateperu-5dfe4-default-rtdb.firebaseio.com';




  Future<List<PacienteModel>> cargarPaciente() async {

    final url  = '$_url/Paciente.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<PacienteModel> pacientes = new List();

      
    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, pacient ){
      print(pacient);
      final pacientTemp = PacienteModel.fromJson(pacient);
      pacientTemp.id = id;

      pacientes.add( pacientTemp );

    });

    // print( noticias[0].id );

    return pacientes;

  }



  


}

