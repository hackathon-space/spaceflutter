// To parse this JSON data, do
//
//     final mapasModel = mapasModelFromJson(jsonString);

import 'dart:convert';

MapasModel mapasModelFromJson(String str) => MapasModel.fromJson(json.decode(str));

String mapasModelToJson(MapasModel data) => json.encode(data.toJson());

class MapasModel {
    MapasModel({
        this.id,
        this.clasificacion,
        this.direccion,
        this.lat,
        this.link,
        this.lng,
        this.nombre,
        this.vacunasPendientes,
        this.poster,
        this.postergrande,
        this.telefono,
        this.vacunados,
        this.nobrevacuna2,
        this.nombrevacuna1,
        this.vacunasdisponibles,
        this.vacunasdisponibles1,
    });
    String id;
    String clasificacion;
    String direccion;
    double lat;
    String link;
    double lng;
    String nombre;
    int vacunasPendientes;
    String poster;
    String postergrande;
    int telefono;
    int vacunados;
    String nobrevacuna2;
    String nombrevacuna1;
    int vacunasdisponibles;
    int vacunasdisponibles1;

    factory MapasModel.fromJson(Map<String, dynamic> json) => MapasModel(
        id                    : json["id"],
        clasificacion         : json["Clasificacion"],
        direccion             : json["Direccion"],
        lat                   : json["Lat"],
        link                  : json["Link"],
        lng                   : json["Lng"],
        nombre                : json["Nombre"],
        vacunasPendientes     : json["VacunasPendientes"],
        poster                : json["Poster"],
        postergrande          : json["Postergrande"],
        telefono              : json["Telefono"],
        vacunados             : json["Vacunados"],
        nobrevacuna2          : json["nobrevacuna2"],
        nombrevacuna1         : json["nombrevacuna1"],
        vacunasdisponibles    : json["vacunasdisponibles"],
        vacunasdisponibles1   : json["vacunasdisponibles1"],
    );

    Map<String, dynamic> toJson() => {
        "id"                  : id,
        "Clasificacion"       : clasificacion,
        "Direccion"           : direccion,
        "Lat"                 : lat,
        "Link"                : link,
        "Lng"                 : lng,
        "Nombre"              : nombre,
        "VacunasPendientes"   : vacunasPendientes,
        "Poster"              : poster,
        "Postergrande"        : postergrande,
        "Telefono    "        : telefono,
        "Vacunados"           : vacunados,
        "nobrevacuna2"        : nobrevacuna2,
        "nombrevacuna1"       : nombrevacuna1,
        "vacunasdisponibles"  : vacunasdisponibles,
        "vacunasdisponibles1" : vacunasdisponibles1,
    };
}
