// To parse this JSON data, do
//
//     final pacienteModel = pacienteModelFromJson(jsonString);

import 'dart:convert';

PacienteModel pacienteModelFromJson(String str) => PacienteModel.fromJson(json.decode(str));

String pacienteModelToJson(PacienteModel data) => json.encode(data.toJson());

class PacienteModel {
    PacienteModel({
        this.id,
        this.alergia1,
        this.alergia2,
        this.alergia3,
        this.apellido,
        this.dni,
        this.direccion,
        this.edad,
        this.fases,
        this.lugardeVacunacion,
        this.nombre,
        this.qr,
        this.tiempoentrevacunas,
        this.dosis,
        this.foto,
        this.nombredevacuna,
    });

    String id;
    String alergia1;
    String alergia2;
    String alergia3;
    String apellido;
    int dni;
    String direccion;
    int edad;
    int fases;
    String lugardeVacunacion;
    String nombre;
    String qr;
    String tiempoentrevacunas;
    int dosis;
    String foto;
    String nombredevacuna;

    factory PacienteModel.fromJson(Map<String, dynamic> json) => PacienteModel(
        id: json["id"],
        alergia1: json["Alergia1"],
        alergia2: json["Alergia2"],
        alergia3: json["Alergia3"],
        apellido: json["Apellido"],
        dni: json["DNI"],
        direccion: json["Direccion"],
        edad: json["Edad"],
        fases: json["Fases"],
        lugardeVacunacion: json["LugardeVacunacion"],
        nombre: json["Nombre"],
        qr: json["Qr"],
        tiempoentrevacunas: json["Tiempoentrevacunas"],
        dosis: json["dosis"],
        foto: json["foto"],
        nombredevacuna: json["nombredevacuna"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "Alergia1": alergia1,
        "Alergia2": alergia2,
        "Alergia3": alergia3,
        "Apellido": apellido,
        "DNI": dni,
        "Direccion": direccion,
        "Edad": edad,
        "Fases": fases,
        "LugardeVacunacion": lugardeVacunacion,
        "Nombre": nombre,
        "Qr": qr,
        "Tiempoentrevacunas": tiempoentrevacunas,
        "dosis": dosis,
        "foto": foto,
        "nombredevacuna": nombredevacuna,
    };

}